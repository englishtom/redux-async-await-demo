/**
 * This is the main entry point of the application, it uses ReactDOM
 * to mount the topmost react component into the container in the dom
 */
import React from 'react';
import ReactDOM from 'react-dom';
import App from './App.jsx';

ReactDOM.render(<App />, document.getElementById('app'));
