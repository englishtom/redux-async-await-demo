import axios from 'axios';

// types
export const LOAD_POSTS = 'LOAD_POSTS'

// action creators
export function loadPosts(page) {
    /**
     * loadPosts would normally return an object however we are returning a function.
     *
     * The function we are returning is tagged with async notifying the middleware that 
     * there will be some promise resolution shiz going on.
     *
     * The function is passed the redux store dispatch method by the middleware
     */
    return async dispatch => {

        // Once the axios request resolves this function is called
        function onSuccess(success) {
            console.log(success); // <- all headers should be accessible here
            const items = success.data;
            // dispatch a regular Action object for the reducer to handle
            dispatch( { 
                type: LOAD_POSTS, 
                posts: {
                    page,
                    items
                }
            })
        }

        // blah, I havent implemented this but it should do the same as onSuccess and
        // dispatch an error Action for a reducer to handle
        function onError(err) {
            console.error(err);
        }

        try {
            // here we tag the axois.get function with await which is == promise.then(func)
            const success = await axios.get('/api/page-' + page + '.json');
            return onSuccess(success);
        } catch(err) {
            return onError(err);
        }

    }
}
